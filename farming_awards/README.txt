Minetest Game mod: farming awards
==========================
See license.txt for license information.

Mod for extending the farming capabilities of minetest. 

Authors of source code
----------------------
ademant (MIT)

Authors of media (textures)
---------------------------
Created by ademant (CC BY 3.0):
  farming_awards_thresher (based on picture from addysgLLGC on opencliparts)
  farming_awards_miller (based on https://openclipart.org/detail/11551/rpg-map-symbols-windmill)
  farming_awards_gatherer (based on picture by Edward Curtis - http://lcweb2.loc.gov/cgi-bin/query/S?pp/ils:@FILREQ(@field(SUBJ+@od1(Mandan+Indians--Subsistence+activities--1900-1910+))+@FIELD(COLLID+ecur)) )
