Minetest Game mod: farming XPFW
==========================
See license.txt for license information.

Mod for extending the farming mod with statistics with xpfw.


Authors of source code
----------------------
ademant (MIT)

Authors of media (textures)
---------------------------
Created by ademant (CC BY 3.0):
  farming_awards_thresher (based on picture from addysgLLGC on opencliparts)
  farming_awards_miller (based on https://openclipart.org/detail/11551/rpg-map-symbols-windmill)
